//Estefan Maheux-saban
//1931517
package geometry;

//declare mandatory methods
public interface Shapes {
	double getArea();
	
	double getPerimeter();
}
