//Estefan Maheux-saban
//1931517
package geometry;

public class Rectangle implements Shapes {
	private double length;
	private double width;
	
	//constructor
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	//getters
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	//returns the area of the rectangle
	public double getArea() {
		return this.length * this.width;
	}
	
	//returns the perimeter of the rectangle
	public double getPerimeter() {
		return (this.length * 2) + (this.width * 2);
	}
class Square extends Rectangle{
		public Square(double sideLength) {
			super(sideLength, sideLength);
		}
	
}
	

}
