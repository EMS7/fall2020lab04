//Estefan Maheux-saban
//1931517
package geometry;
import java.lang.Math;

public class Circle implements Shapes {
	private double radius;
	
	//constructor
	public Circle(double radius) {
		this.radius = radius;
	}
	
	//getters
	public double getRadius() {
		return this.radius;
	}
	
	//return area of circle
	public double getArea() {
		return Math.PI * Math.pow(this.radius, 2);
	}
	
	//return perimeter/circumference of circle
	public double getPerimeter() {
		return 2.0 * Math.PI * this.radius;
	}
}
