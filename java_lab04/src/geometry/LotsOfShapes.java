//Estefan Maheux-saban
//1931517
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shapes[] shape_arr = new Shapes[5];
		shape_arr[0] = new Rectangle(4.0, 6.0);
		shape_arr[1] = new Rectangle(8.0, 12.0);
		shape_arr[2] = new Circle(5.0);
		shape_arr[3] = new Circle(10.0);
		shape_arr[4] = new Rectangle(5.0, 5.0);
		for(Shapes i: shape_arr) {
			System.out.println("Area: " + i.getArea());
			System.out.println("Peremeter: "+ i.getPerimeter());
		}
		
	}

}
