//Estefan Maheux-saban
//1931517

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book[] book_arr = new Book[5];
		book_arr[0] = new Book("How to play fortnite", "Ninja");
		book_arr[1] = new ElectronicBook("Behind the Snake Eater", "David Hayter", 1000);
		book_arr[2] = new Book("Computer Science 101", "Mister Java");
		book_arr[3] = new ElectronicBook("Why are people mean to me :(", "Mister Sad", 650);
		book_arr[4] = new ElectronicBook("Electric Cars", "Elon Musk", 2500);
		for (Book i : book_arr) {
			System.out.println(i.toString());
		}
	}

}
