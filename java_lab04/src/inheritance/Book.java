//Estefan Maheux-saban
//1931517
package inheritance;

public class Book {
	protected String title;
	private String author;
	
	//constructor
	public Book(String title ,String author) {
		this.title = title;
		this.author = author;
	}
	
	//getters
	public String getTitle() {
		return this.title;
	}
	public String getAuthor() {
		return this.author;
	}
	
	//toString method
	@Override
	public String toString() {
		return "Title Of Book: " + this.title  + ",Author Of Book: " + this.author; 
	}
}
	class ElectronicBook extends Book{
	private int numberOfBytes;
		
		//constructor
	public ElectronicBook(String title ,String author ,int numberOfBytes) {
		super(title, author);
		this.numberOfBytes = numberOfBytes;
	}
		
	@Override
	public String toString() {
	return "Number Of Bytes: " + this.numberOfBytes + ", " + super.toString();
	}
}
	

